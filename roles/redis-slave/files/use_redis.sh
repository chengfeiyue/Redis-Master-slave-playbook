#!/bin/bash
if [ `id -u` -eq 0 ];then
	echo "当前用户为：root用户!，请用cifiadmin用户启动服务"
	exit
fi
case $1 in
  start)
      if [ $(ps -ef | grep redis-server | grep -v grep | wc -l) -eq 0 ]; then
        /app/redis/bin/redis-server /app/redis/etc/redis.conf
        sleep 1
        if [ $(ps -ef | grep redis-server | grep -v grep | wc -l) -gt 0 ]; then
          echo "Start redis-server success, PID is $(cat /app/redis/run/redis_6379.pid)"
        fi
      else
        echo "Redis has been started,PID is $(cat /app/redis/run/redis_6379.pid)"
      fi
  ;;
  stop)
     if [ $(ps -ef | grep redis-server | grep -v grep | wc -l) -lt 1 ]; then
       echo "redis server has not start."
     else
       pkill redis-server
       sleep 1
       if [ $(ps -ef | grep redis-server | grep -v grep | wc -l) -lt 1 ]; then
          echo "Stop redis-server success."
       fi
     fi
  ;;
  restart)
    if [ $(ps -ef | grep redis-server | grep -v grep | wc -l) -lt 1 ]; then
       echo "redis server has not start."
    else
      pkill redis-server
      sleep 1
      if [ $(ps -ef | grep redis-server | grep -v grep | wc -l) -lt 1 ]; then
        echo "Stop redis-server success."
      fi
      /app/redis/bin/redis-server /app/redis/etc/redis.conf
      if [ $(ps -ef | grep redis-server | grep -v grep | wc -l) -gt 0 ]; then
        echo "Start redis-server success, PID is $(cat /app/redis/run/redis_6379.pid)"
      fi
    fi 
  ;;
  status)
    if [ $(ps -ef | grep redis-server | grep -v grep | wc -l) -eq 0 ]; then
      echo "redis-server has not been start."
    else
      echo "redis-server is start. PID is $(cat /app/redis/run/redis_6379.pid)"
    fi
  ;;
  *)
      echo "Use start|restart|stop|status"
  ;;
esac
